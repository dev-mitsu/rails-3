country = "Japan"

case country
when "USA"
  puts "Hello!"
when "Japan"
  puts "こんにちは!"
when "France"
  puts "Bonjour!"
else
  puts "ヘイ！"
end

# case 対象オブジェクト
# when 値1
#   値1と一致する場合に行う処理
# when 値2
#   値2と一致する場合に行う処理
# when 値3
#   値3と一致する場合に行う処理
# else
#   どの値にも一致しない場合に行う処理
# end
